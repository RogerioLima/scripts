# Scripts for use in remote applications

---

## deployForNodeApplications
#### - Script used to deploy a NODEjs application on teresa

* Import to remote application
  - In the package.json file add the line  
  `"curl -LsS https://gitlab.com/RogerioLima/scripts/raw/master/deployForNodeApplications > scripts/deploy && bash scripts/deploy"`
  
  
- Utilization in remote application
  - Run the command  
  `npm run deploy .env.staging teresa-pod-name`  



* Instructions
```
The script will build the source code, load the .env.* file, extract the variables and apply them to Teresa.
Soon after will deploy to Teresa.

PS:

* Variables that contain white space in their content must also be entered without single quotes
    Ex: variable=value with space


* The contents of the .env.* file must contain the keys:
      #### SECRET VARIABLES ####
      #### ENV VARIABLES ####
    no matter the order.
    And the ############### key to indicate the end of the file
    
    Everything below #### SECRET VARIABLES #### will be applied as a secret variable.
    
    Everything below #### ENV VARIABLES #### will be applied as a normal variable.


* The .env.* file must be in the following format:

      **** Staging env example ***
      Filename: .env.staging
      
      Content:
      #### SECRET VARIABLES ####
      NAME_SECRET_VARIABLE_1=value
      NAME_SECRET_VARIABLE_2=value
      
      #### ENV VARIABLES ####
      NAME_ENV_VARIABLE_1=value
      NAME_ENV_VARIABLE_2=value
     
      ###############


      
      **** Production env example ***
      Filename: .env.production
      
      Content:
      #### SECRET VARIABLES ####
      NAME_SECRET_VARIABLE_1=value
      NAME_SECRET_VARIABLE_2=value
      
      #### ENV VARIABLES ####
      NAME_ENV_VARIABLE_1=value
      NAME_ENV_VARIABLE_2=value
     
      ###############
```

---